<?php

namespace Omnipay\placetopay\Message;

use Omnipay\Common\Message\AbstractResponse;
use Omnipay\Common\Message\RequestInterface;
use Omnipay\Common\Exception\InvalidResponseException;

/**
 * placetopay Response
 */
class Response extends AbstractResponse
{   
    public $response;

    public function __construct(RequestInterface $request, $response)
    {
        $this->request = $request;
        $this->data = $response->toArray();
        $this->response = $response;
    }

    /**
     * Does the response require a redirect?
     *
     * @return boolean
     */
    public function isRedirect()
    {
        return true;
    }

    public function getRedirectUrl()
    {
        return $this->response->processUrl();
    }

    public function isSuccessful()
    {
        return false;
    }

    public function getTransactionReference()
    {
        return $this->response->requestId();
    }

    public function getMessage()
    {
        return $this->response->requestId();
    }

    public function getCardReference()
    {
        return null;
    }

    public function getCode()
    {
        return $this->response->isSuccessful();
    }
}
