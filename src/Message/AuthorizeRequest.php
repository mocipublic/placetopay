<?php

namespace Omnipay\placetopay\Message;

use Omnipay\Common\Message\AbstractRequest;

/**
 * placetopay Authorize Request
 *
 * ### Example
 *
 * <code>
 * // Create a gateway for the placetopay pro Gateway
 * // (routes to GatewayFactory::create)
 * $gateway = Omnipay::create('placetopay_Pro');
 *
 * // Initialise the gateway
 * $gateway->initialize(array(
 *     'username'       => $myusername,
 *     'password'       => $mypassword,
 *     'vendor'         => $mymerchantid,
 *     'partner'        => $PayPalPartner,
 *     'testMode'       => true, // Or false for live transactions.
 * ));
 *
 * // Create a credit card object
 * // This card can be used for testing.
 * $card = new CreditCard(array(
 *             'firstName'    => 'Example',
 *             'lastName'     => 'Customer',
 *             'number'       => '4111111111111111',
 *             'expiryMonth'  => '01',
 *             'expiryYear'   => '2020',
 *             'cvv'          => '123',
 * ));
 *
 * // Do an authorize transaction on the gateway
 * $transaction = $gateway->authorize(array(
 *     'amount'                   => '10.00',
 *     'currency'                 => 'AUD',
 *     'card'                     => $card,
 * ));
 * $response = $transaction->send();
 * if ($response->isSuccessful()) {
 *     echo "Authorize transaction was successful!\n";
 *     $sale_id = $response->getTransactionReference();
 *     echo "Transaction reference = " . $sale_id . "\n";
 * }
 * </code>
 */
class AuthorizeRequest extends AbstractRequest
{
    protected $liveEndpoint = 'https://secure.placetopay.com/redirection/';
    protected $testEndpoint = 'https://test.placetopay.com/redirection/';
    protected $action = 'A';

    /**
     * Get the username.
     *
     * This is the ID that you specified when you got the placetopay account.
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->getParameter('username');
    }

    /**
     * Set the username.
     *
     * This is the ID that you specified when you got the placetopay account.
     *
     * @param string $value
     * @return AuthorizeRequest provides a fluent interface.
     */
    public function setUsername($value)
    {
        return $this->setParameter('username', $value);
    }

    /**
     * Get the password.
     *
     * This is the password that you specified when you got the placetopay account.
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->getParameter('password');
    }

    /**
     * Set the password.
     *
     * This is the password that you specified when you got the placetopay account.
     *
     * @param string $value
     * @return AuthorizeRequest provides a fluent interface.
     */
    public function setPassword($value)
    {
        return $this->setParameter('password', $value);
    }

    /**
     * Get the vendor.
     *
     * The ID that you specified when you got the placetopay account, the same as the username unless you
     * have created additional users on the account. That is, the merchant login ID for the account.
     *
     * @return string
     */
    public function getVendor()
    {
        return $this->getParameter('vendor');
    }

    /**
     * Set the vendor.
     *
     * The ID that you specified when you got the placetopay account, the same as the username unless you
     * have created additional users on the account. That is, the merchant login ID for the account.
     *
     * @param string $value
     * @return AuthorizeRequest provides a fluent interface.
     */
    public function setVendor($value)
    {
        return $this->setParameter('vendor', $value);
    }

    /**
     * Get the partner.
     *
     * The placetopay partner. This may be PayPal, or if an account was provided by an authorized PayPal
     * reseller, who registered a placetopay user, then the ID provided by the reseller is used.
     *
     * @return string
     */
    public function getPartner()
    {
        return $this->getParameter('partner');
    }

    /**
     * Set the partner.
     *
     * The placetopay partner. This may be PayPal, or if an account was provided by an authorized PayPal
     * reseller, who registered a placetopay user, then the ID provided by the reseller is used.
     *
     * @param string $value
     * @return AuthorizeRequest provides a fluent interface.
     */
    public function setPartner($value)
    {
        return $this->setParameter('partner', $value);
    }

    public function getComment1()
    {
        return $this->getDescription();
    }

    public function setComment1($value)
    {
        return $this->setDescription($value);
    }

    public function getComment2()
    {
        return $this->getParameter('comment2');
    }

    public function setComment2($value)
    {
        return $this->setParameter('comment2', $value);
    }

    public function getOrderId()
    {
        return $this->getParameter('orderid');
    }

    public function setOrderId($value)
    {
        return $this->setParameter('orderid', $value);
    }

    public function setPoNum($value)
    {
        return $this->setParameter('ponum', $value);
    }

    public function getPoNum()
    {
        return $this->getParameter('ponum');
    }

    /**
     * @deprecated
     */
    public function getOrigid()
    {
        return $this->getParameter('origid');
    }
    
    /**
     * @deprecated
     */
    public function setOrigid($value)
    {
        return $this->setParameter('origid', $value);
    }    

    /**
     * @deprecated
     */
    public function getDocument()
    {
        return $this->getParameter('document');
    }

    public function setDocument($value)
    {
        return $this->setParameter('document', $value);
    }

    /**
     * @deprecated
     */
    public function getTypeDocument()
    {
        return $this->getParameter('typeDocument');
    }

    public function setTypeDocument($value)
    {
        return $this->setParameter('typeDocument', $value);
    }

    /**
     * @deprecated
     */
    public function getPayerIsBuyer()
    {
        return $this->getParameter('payerIsBuyer');
    }

    public function setPayerIsBuyer($value)
    {
        return $this->setParameter('payerIsBuyer', $value);
    }

    /**
     * @deprecated
     */
    public function getLastName()
    {
        return $this->getParameter('lastname');
    }

    public function setLastName($value)
    {
        return $this->setParameter('lastname', $value);
    }


        /**
     * @deprecated
     */
    public function getEmail()
    {
        return $this->getParameter('email');
    }

    public function setEmail($value)
    {
        return $this->setParameter('email', $value);
    }

            /**
     * @deprecated
     */
    public function getMobile()
    {
        return $this->getParameter('mobile');
    }

    public function setMobile($value)
    {
        return $this->setParameter('mobile', $value);
    }

                /**
     * @deprecated
     */
    public function getCancelUrl()
    {
        return $this->getParameter('cancelUrl');
    }

    public function setCancelUrl($value)
    {
        return $this->setParameter('cancelUrl', $value);
    }

    /**
     * @deprecated
     */
/*     public function getCurrency($value)
    {
        return $this->getParameter('currency');
    } */
    

    protected function getBaseData()
    {
    
        $data = array();
        $data['TRXTYPE'] = $this->action;
        $data['USER'] = $this->getUsername();
        $data['PWD'] = $this->getPassword();
        $data['VENDOR'] = $this->getVendor();
        $data['PARTNER'] = $this->getPartner();
        if ($this->getDescription()) {
            $data['COMMENT1'] = $this->getDescription();
        }
        if ($this->getComment2()) {
            $data['COMMENT2'] = $this->getComment2();
        }

        return $data;
    }

    public function getData()
    {
        //DATA PLACETOPAY
        $buyer = [
                "name" => $this->getUsername(),
                "surname" => $this->getLastName(),
                "email" => $this->getEmail(),
                "documentType" => $this->getTypeDocument(),
                "document" => (int)$this->getDocument(),
                "mobile" => (int)$this->getMobile(),
        ];

        $payer = $this->getPayerIsBuyer() ? $buyer: [];

        $payment = [
                'reference' => $this->getOrderId(),
                'description' => $this->getDescription(),
                'amount' => [
                    'currency' => $this->getCurrency(),
                    'total' => $this->getAmount(),
                ],
        ];


        $data = [
    
        'buyer' => $buyer,
        'payer' => $payer, 
        'payment' => $payment,
        'expiration' => date('c', strtotime('+1 days')),
        'returnUrl' => $this->getReturnUrl(),
        "cancelUrl" => $this->getCancelUrl(),
        // 'ipAddress' => '127.0.0.1',
        'userAgent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36',
        ];
        return $data;
    }

    public function sendData($data)
    {


        $placetopay = new \Dnetix\Redirection\PlacetoPay([
            'login' => 'ff684c45a63f769d824994dcc1369fb9',
            'tranKey' => 'X1GIXSF2Dxtq0bfg',
            'url' => 'https://secure.placetopay.com/redirection/',
            'type' => \Dnetix\Redirection\PlacetoPay::TP_REST,

        ]);

        $reference = '123456789';
        $request = $data;

        $response = $placetopay->request($request);

       /*  $httpResponse = $this->httpClient->request(
            'POST',
            $this->getEndpoint(),
            [],
            json_encode($data) //$this->encodeData($data)
        ); */

        return $this->response = new Response($this, $response);
    }

    /**
     * Encode absurd name value pair format
     */
    public function encodeData(array $data)
    {
        return $data;
        $output = array();
        foreach ($data as $key => $value) {
            $output[] = $key.'['.strlen($value).']='.$value;
        }

        return implode('&', $output);
    }

    protected function getEndpoint()
    {
        return $this->getTestMode() ? $this->testEndpoint : $this->liveEndpoint;
    }
}
